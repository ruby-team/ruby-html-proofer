.TH "HTMLProofe" "1" "2019-03-24" "" "User commands"

.SH NAME
htmlproofer \- validate rendered HTML files 

.SH SYNOPSIS
.B htmlproofer
.I directory
.RB [ options ]

.SH DESCRIPTION
.B htmlproofer
is a set of tests to validate HTML output. These tests check if image
references are legitimate, if they have alt tags, if internal links are
working, and so on.
.B HTMLProofer 
can run on a file, a directory, an array of directories, or an array of
links. Below is a mostly comprehensive list of checks that it can perform.

.PP
Images (\fB<img>\fR elements)
.IP \[bu]
Whether all images have alt tags
.IP \[bu]
Whether internal image references are not broken
.IP \[bu]
Whether external images are showing
.IP \[bu]
Whether images are HTTPS

.PP
Links (\fB<a>\fR, \fB<link>\fR elements)
.IP \[bu]
Whether internal links are working
.IP \[bu]
Whether internal hash references (#linkToMe) are working
.IP \[bu]
Whether external links are working
.IP \[bu]
Whether links are HTTPS
.IP \[bu]
Whether CORS/SRI is enabled

.PP
Scripts (\fB<script>\fR elements)
.IP \[bu]
Whether internal script references are working
.IP \[bu]
Whether external scripts are loading
.IP \[bu]
Whether CORS/SRI is enabled

.PP
Favicon
.IP \[bu]
Whether favicons are valid.

.PP
OpenGraph
.IP \[bu]
Whether the images and URLs in the OpenGraph metadata are valid.

.PP
HTML
.IP \[bu]
Whether your HTML markup is valid.

This is done via Nokogiri to ensure well-formed markup.

.SH OPTIONS
.PP
Listed below are the command line options for \fBhtmlproofer\fR:
.TP
.B \-\-allow-missing-href
Don't flag tags missing an \fBhref\fR attribute. This is the default for
HTML5.
.TP
.B \-\-allow-hash-href
Ignores \fBhref="#"\fR.
.TP
.B \-\-as-links
Assumes that \fIPATH\fR is a comma-separated array of links to check.
.TP
.B \-\-alt-ignore \fIimage1,[image2,...]\fR
A comma-separated list of Strings or RegExps containing images whose
missing \fBalt\fR attributes are safe to ignore.
.TP
.B \-\-assume-extension
Automatically add extension (e.g. \fB.html\fR) to file paths, to allow
extensionless URLs (as supported by Jekyll 3 and GitHub Pages).
.TP
.B \-\-checks-to-ignore \fIcheck1,[check2,...]\fR
An array of Strings indicating which checks not to perform.
.TP
.B \-\-check-external-hash
Checks whether external hashes exist (even if the webpage exists). This slows
the checker down.
.TP
.B \-\-check-favicon
Enables the favicon checker.
.TP
.B \-\-check-html
Enables HTML validation errors from Nokogiri.
.TP
.B \-\-check-img-http
Check, that images use \fBHTTPS\fR.
.TP
.B \-\-check-opengraph
Enables the Open Graph checker.
.TP
.B \-\-check-sri
Check that \fB<link>\fR and \fB<script>\fR external resources do use SRI.
.TP
.B \-\-directory-index-file \fIfilename\fR
Sets the file to look for when a link refers to a directory. Defaults
to \fIindex.html\fR.
.TP
.B \-\-disable-external
Don't run the external link checker, which can take a lot of time.
.TP
.B \-\-empty-alt-ignore
If \fItrue\fR, ignores images with empty \fBalt\fR attribues.
.TP
.B \-\-error-sort \fIsort\fR
Defines the sort order for error output. Can be \fI:path\fR, \fI:desc\fR,
or \fI:status\fR. Defaults to \fI:path\fR.
.TP
.B \-\-enforce-https
Fails if a link is not marked as HTTPS.
.TP
.B \-\-extension \fIext\fR
The extension of HTML files including the dot. Defaults to \fI.html\fR.
.TP
.B \-\-external_only
Only check for problems with external references.
.TP
.B \-\-file-ignore \fIfile1,[file2,...]\fR
A comma-separated list of Strings or RegExps containing file paths that
are safe to ignore.
.TP
.B \-\-help
Print this usage information on the command line.
.TP
.B \-\-http-status-ignore \fI123,[xxx, ...]\fR
A comma-separated list of numbers representing status codes to ignore.
.TP
.B \-\-internal-domains \fIdomain1,[domain2,...]\fR
A comma-separated list of Strings containing domains that will be treated
as internal urls.
.TP
.B \-\-report-invalid-tags
Ignore errors from \fB--check-html\fR associated with unknown markup.
.TP
.B \-\-report-missing-names
Ignore errors from \fB--check-html\fR associated with missing entities.
.TP
.B \-\-report-script-embeds
Ignore errors from \fB--check-html\fR associated with \fB<script>\fRs.
.TP
.B \-\-log-level \fIlevel\fR
Sets the logging level, as determined by Yell. One
of \fI:debug\fR, \fI:info\fR, \fI:warn\fR, \fI:error\fR, or \fI:fatal\fR.
Defaults to \fI:info\fR.
.TP
.B \-\-only-4xx
Only reports errors for links that fall within the 4xx status code range
.TP
.B \-\-storage-dir \fIdirectory\fR
Directory where to store the cache log. Defaults to \fItmp/.htmlproofer\fR.
.TP
.B \-\-timeframe \fItime\fR
A string representing the caching timeframe.
.TP
.B \-\-typhoeus-config \fIstring\fR
JSON-formatted string of Typhoeus config. It will override
the \fBhtml-proofer\fR defaults.
.TP
.B \-\-url-ignore \fIlink1,[link2,...]\fR
A comma-separated list of Strings or RegExps containing URLs that are safe to
ignore. It affects all HTML attributes. Non-HTTP(S) URIs are always ignored.
.TP
.B \-\-url-swap \fIre:string,[re:string,...]\fR
A comma-separated list containing key-value pairs of \fIRegExp => String\fR.
It transforms URLs that match \fIRegExp\fR into \fIString\fR via \fBgsub\fR.
The escape sequences \fI\\:\fR should be used to produce literal \fI:\fRs.'

.SS Special usage cases
.PP
For options which require an array of input, values can be surrounded by quotes.
Don't use any spaces. For example, to exclude an array of HTTP status code:
.PP
.RS 4
.B htmlproofer --http-status-ignore "999,401,404" ./out
.RE
.PP
For options like \fB--url-ignore\fR, which require an array of regular
expressions, the following syntax works:
.PP
.RS 4
.B htmlproofer --url-ignore "/www.github.com/,/foo.com/" ./out
.RE
.PP
The \fB--url-swap\fR switch is a bit special, since one will pass in a pair
of \fIRegEx:String\fR values. The escape sequences \fI\\:\fR should be used
to produce literal \fI:\fR. \fBhtmlproofer\fR will figure out what you mean.
.PP
.RS
.B htmlproofer --url-swap "wow:cow,mow:doh" --extension .html.erb --url-ignore www.github.com ./out
.RE

.SH AUTHORS
The program author is \fBGaren Torikian\fR.
.PP
This manual page page was written
by \fBDaniel Leidert\fR <\&daniel.leidert@wgdd.de\&>
for the Debian distribution (but may be used by others).

