require 'gem2deb/rake/spectask'

ENV['LC_ALL'] = 'C.UTF-8'
ENV['LANG']   = 'C.UTF-8'

Gem2Deb::Rake::RSpecTask.new do |spec|
  spec.pattern = './spec/**/*_spec.rb'
end
